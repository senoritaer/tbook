import json

def main():
    with open('assets/data.json','r',encoding='utf-8') as f:
        a = f.read()

    d = json.loads(a)
    print(d[0])
    count = len(d)
    n = (count - 1) // 15 + 1
    for i in range(n):
        with open(f'src/{i+1}.md','w',encoding='utf-8') as f:
            for j in range(15):
                t = i * 15 + j
                if t >= count:
                    return
                f.write('![]({})\n'.format(d[t]['url']))
main()